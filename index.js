const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');
const routes = require('./routes');
const serverConfig = require('./config/server.config.json');

mongoose.Promise = Promise;
mongoose.set('useCreateIndex', true);

mongoose
  .connect(
    "mongodb://localhost:27017/back-test",
    { useNewUrlParser: true, reconnectTries: 5 }
  )
  .then(() => {
    const app = express();

    app.use(morgan('dev'));
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(bodyParser.json());
    app.use(routes);

    app.use((err, req, res, next) => {
      console.log('err', err);
      res
        .status(err.statusCode || 500)
        .json(err.message);
    });

    app.listen(serverConfig.port, () => {
      console.log(`App listening on port ${serverConfig.port}!`);
    });
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });