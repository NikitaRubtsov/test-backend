const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const serverConfig = require('../config/server.config.json');

module.exports.signup = async body => {
  const userInfo = Object.assign({}, body);

  const hashPassword = await bcrypt.hash(userInfo.password, saltRounds);
  if(hashPassword) {
    userInfo.password = hashPassword;
    return User.create(userInfo);
  } else {
    const error = new Error();
    error.statusCode = 401;
    error.message = 'Didn\'t create pass';

    throw error;
  }
};

module.exports.auth = async body => {
  const user = await User.findOne({email: body.email});

  if (!user) {
    const error = new Error();
    error.statusCode = 401;
    error.message = 'Isn\'t exist';

    throw error;
  } else {
    return new Promise(resolve => {
      jwt.sign(
        { data: user.email },
        serverConfig.tokenSecret,
        { expiresIn: '1h' },
        (error, token) => {
          resolve(token);
        });
    });
  }
};