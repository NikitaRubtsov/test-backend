const express = require('express');
const router = express.Router();
const authMiddleware = require('./middleware/auth');

const product = require('./controllers/products.controller');
const order = require('./controllers/orders.controller');
const user = require('./controllers/user.controller');

router.get('/', (req, res) => {
  res.send('Hello World!');
});

router.post('/user/auth', user.auth);
router.post('/user/signup', user.signup);

router.use(authMiddleware);

router.post('/product', product.createProduct);
router.delete('/product/:id', product.deleteProduct);
router.put('/product/:id', product.editProduct);
router.get('/product/:id', product.readProduct);
router.get('/products', product.getProducts);

router.post('/order', order.createOrder);
router.delete('/order/:id', order.deleteOrder);
router.put('/order/:id', order.editOrder);
router.get('/order/:id', order.readOrder);
router.get('/orders', order.getOrders);


router.use((req, res) => {
  res
    .status(404)
    .send('Page not found.');
});

router.use((err, req, res) => {
  res.locals.message = err.message;
  res.locals.error = err;
  // if (res.statusCode === 200) { res.status(err.status || 500); }

  res.json({ error: res.locals.message });
});


module.exports = router;