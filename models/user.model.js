const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: String,
});

module.exports = mongoose.model('User', user);