const Product = require('../models/product.model');

module.exports.get = () => {
  return Product.find({});
};

module.exports.create = body => {
  return Product.create(body);
};

module.exports.deleteOne = id => {
  return Product.findByIdAndRemove(id);
};

module.exports.edit = (id, body) => {
  return Product.findByIdAndUpdate(id, body);
};

module.exports.read = id => {
  return Product.findById(id);
};