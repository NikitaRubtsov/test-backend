const userService = require('../services/user.service');

module.exports.signup = async(req, res, next) => {
  const { body } = req;

  try {
    await userService.signup(body);
    res.status(200).send('User has been created');
  } catch (error) {
    next(error);
  }
};

module.exports.auth = async(req, res, next) => {
  const { body } = req;

  try {
    const user = await userService.auth(body);
    console.log('user', user);
    res.status(200).json(user);
  } catch (error) {
    next(error);
  }
};