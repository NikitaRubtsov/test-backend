const productService = require('../services/product.service');

module.exports.getProducts = async(req, res, next) => {
  try {
    const products = await productService.get();
    res.status(200).json(products);
  } catch (error) {
    next(error);
  }
};

module.exports.createProduct = async(req, res, next) => {
  const { body } = req;

  try {
    await productService.create(body);
    res.status(201).send('Product has been created');
  } catch (error) {
    next(error);
  }
};

module.exports.deleteProduct = async(req, res, next) => {
  const { id } = req.params;

  try {
    await productService.deleteOne(id);
    res.status(200).send('Product has been removed');
  } catch (error) {
    next(error);
  }
};

module.exports.editProduct = async(req, res, next) => {
  const { id } = req.params;
  const { body } = req;

  try {
    await productService.edit(id, body);
    res.status(200).send('Product has been updated');
  } catch (error) {
    next(error);
  }
};

module.exports.readProduct = async (req, res, next) => {
  const { id } = req.params;

  try {
    const product = await productService.read(id);
    res.status(200).json(product);
  } catch (error) {
    next(error);
  }
};