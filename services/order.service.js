const Order = require('../models/order.model');

module.exports.get = () => {
  return Order.find({});
};

module.exports.read = id => {
  return Order.findById(id);
};

module.exports.create = (quantity, productId) => {
  return Order.create({
    quantity,
    product: productId
  });
};

module.exports.deleteOne = id => {
  return Order.deleteOne({_id: id});
};

module.exports.edit = (id, body) => {
  return Order.findByIdAndUpdate(id, body);
};