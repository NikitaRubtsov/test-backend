const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const product = new Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },
  price: Number,
  description: String,
});

module.exports = mongoose.model('Product', product);