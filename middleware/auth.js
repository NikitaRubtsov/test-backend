const jwt = require('jsonwebtoken');
const serverConfig = require('../config/server.config.json');

function auth(req, res, next) {
  try {
    const { token } = req.headers;

    jwt.verify(token, serverConfig.tokenSecret, (error, decoded) => {
      if(error) {
        throw new Error('Unauthorized');
      }
      req.user = decoded;
      next();
    });
  } catch (error) {
    error.message = 'Unauthorized';
    error.status = 401;
    console.log('error', error);
    next(error);
  }
}

module.exports = auth;