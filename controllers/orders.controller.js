const orderService = require('../services/order.service');

module.exports.deleteOrder =  async (req, res, next) => {
  const { id } = req.params;

  try {
    await orderService.deleteOne(id);
    console.log(`${id} has been deleted`);
    res.status(200).send(`${id} has been deleted`)
  } catch (error) {
    next(error);
  }
};

module.exports.createOrder = async (req, res, next) => {
  const { quantity, productId } = req.body;

  try {
    const result = await orderService.create(quantity, productId);
    console.log('Good, order is created');
    res.status(201).json(result)
  } catch (error) {
    next(error);
  }
};

module.exports.editOrder = async (req, res, next) => {
  const { id } = req.params;
  const { body } = req;

  try {
    await orderService.edit(id, body);
    res.status(200).send('Order edit is fine');
  } catch (error) {
    next(error);
  }
};

module.exports.getOrders = async (req, res, next) => {
  try {
    const orders = await orderService.get();
    res.status(200).json(orders);
  } catch (error) {
    next(error);
  }
};

module.exports.readOrder = async (req, res, next) => {
  const { id } = req.params;

  try {
    const order = await orderService.read(id);
    res.status(200).json(order);
  } catch (error) {
    next(error);
  }
};